const express = require('express');
const getUserInfo = require('../controllers/getUserInfo');
const deleteUser = require('../controllers/deleteUser');
const updateUserPassword = require('../controllers/updateUserPassword');
const userRouter = new express.Router();

userRouter.get('/me', getUserInfo);
userRouter.delete('/me', deleteUser);
userRouter.patch('/me', updateUserPassword);

module.exports = userRouter;
