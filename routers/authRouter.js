const express = require('express');
const userRegistration = require('../controllers/userRegistration');
const userLogin = require('../controllers/userLogin');
const validationMiddleware = require('../middleware/validationMiddleware');
const asyncWrapper = require('../routers/heplers/asyncWrapper');
const authRouter = new express.Router();


authRouter.post('/register',
    asyncWrapper(validationMiddleware), userRegistration);
authRouter.post('/login', userLogin);

module.exports = authRouter;
