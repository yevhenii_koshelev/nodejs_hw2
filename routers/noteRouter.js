const express = require('express');
const createNote = require('../controllers/createNote');
const getAllNotes = require('../controllers/getAllNotes');
const getNote = require('../controllers/getNote');
const updateNote = require('../controllers/updateNote');
const switchNote = require('../controllers/switchNote');
const deleteNote = require('../controllers/deleteNote');
const checkNote = require('../middleware/checkNote');
const noteValidationMiddleware = require(
    '../middleware/noteValidationMiddleware');
const noteRouter = new express.Router();


noteRouter.get('/', getAllNotes);
noteRouter.post('/', noteValidationMiddleware, createNote);
noteRouter.get('/:id', checkNote, getNote);
noteRouter.put('/:id', checkNote, updateNote);
noteRouter.patch('/:id', checkNote, switchNote);
noteRouter.delete('/:id', checkNote, deleteNote);

module.exports = noteRouter;
