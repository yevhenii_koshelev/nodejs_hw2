module.exports = getNote = (req, res) => {
  try {
    const {note} = req;
    res.status(200).json({note});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};

