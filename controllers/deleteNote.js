module.exports = deleteNote = async (req, res) => {
  try {
    const {note} = req;
    await note.delete();
    res.status(200).json({message: `Success`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
