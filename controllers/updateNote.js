module.exports = updateNote = async (req, res) => {
  const {text} = req.body;
  const {note} = req;
  if (!text.trim().length) {
    return res.status(400).json({message: 'Enter description of your note'});
  }
  try {
    note.text = req.body.text;
    await note.save();
    res.status(200).json({message: `Success`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
