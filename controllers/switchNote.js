module.exports = switchNote = async (req, res) => {
  try {
    const {note} = req;
    note.completed = !note.completed;
    await note.save();
    res.status(200).json({message: `Success`});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
