const {Note} = require('../models/noteModel');

module.exports = getAllNotes = async (req, res) => {
  const userId = req.user.id;
  const {skip = 0, limit = 0} = req.query;
  try {
    const notes = await Note.find({userId})
        .limit(+limit)
        .skip(+skip);
    res.status(200).json({notes});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
