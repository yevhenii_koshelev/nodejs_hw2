const {User} = require('../models/userModel');

module.exports = deleteUser = async (req, res) => {
  const {id, username} = req.user;
  try {
    await User.findByIdAndRemove(id);
    res.status(200).json({
      message: `User ${username} deleted successfully`,
    });
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
