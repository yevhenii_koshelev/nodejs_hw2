const {Note} = require('../models/noteModel');

module.exports = createNote = async (req, res) => {
  const note = new Note({
    userId: req.user.id,
    text: req.body.text,
  });

  try {
    await note.save();
    res.status(200).json({message: 'Success'});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
