const {hashingPassword} = require('../utils/encrypting');
const {User} = require('../models/userModel');

module.exports = userRegistration = async (req, res) => {
  const {username, password} = req.body;
  try {
    const hashPassword = await hashingPassword(password);
    const user = new User({
      username: username,
      password: hashPassword,
    });
    await user.save();

    res.status(200).json({
      message: `User ${username} created successfully!`,
    });
  } catch (err) {
    if (err.name === 'ValidationError') {
      return res.status(400).json({message: err});
    }
    res.status(500).json({message: err});
  }
};
