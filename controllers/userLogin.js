const userIsExists = require('../middleware/userIsExistsMiddlware');
const {comparisonPassword} = require('../utils/encrypting');
const jwt = require('jsonwebtoken');
const JWT_SECRET = process.env.JWT_SECRET;

module.exports = userLogin = async (req, res) => {
  const {username, password} = req.body;
  try {
    const existUser = await userIsExists(username);
    if (!existUser) {
      return res.status(400).json({
        message: `No user with username ${username} found`,
      });
    }
    const comparePassword = comparisonPassword(password, existUser.password);

    if (!comparePassword) {
      return res.status(400).json({message: `Wrong username or password`});
    }

    const token = jwt.sign({
      username: existUser.username,
      id: existUser._id,
    }, JWT_SECRET);
    res.status(200).json({message: `Login successfully`, jwt_token: token});
  } catch (err) {
    res.status(500).json({message: err.message});
  }
};
