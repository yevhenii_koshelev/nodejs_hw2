const Joi = require('joi');

module.exports = noteSchema = Joi.object({
  text: Joi.string()
      .min(1)
      .required(),
});
