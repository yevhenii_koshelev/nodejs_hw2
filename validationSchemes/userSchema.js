const Joi = require('joi');

module.exports = userSchema = Joi.object({
  username: Joi.string()
      .alphanum()
      .min(1)
      .max(30)
      .required(),

  password: Joi.string()
      .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),

});
