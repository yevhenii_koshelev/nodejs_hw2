require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');
const authMiddleware = require('./middleware/authMiddleware');
const {MONGOLOG, MONGOPASS, PORT} = process.env;

const app = express();

app.use(express.json());
app.use(morgan('tiny'));
app.use('/api/users', authMiddleware, userRouter);
app.use('/api/auth', authRouter);
app.use('/api/notes', authMiddleware, noteRouter);
app.use((err, req, res, next) => {
  res.status(500).json({message: err.message});
});


const start = async () => {
  await mongoose.connect(`mongodb+srv://${MONGOLOG}:${MONGOPASS}@nodejshw2.bmfra.mongodb.net/nodejs_hw2?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });


  app.listen(PORT, () => {
    console.log(`Server started at port: ${PORT}`);
  });
};

start();
