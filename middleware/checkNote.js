const {Note} = require('../models/noteModel');

module.exports = checkNote = async (req, res, next) => {
  const {id} = req.params;
  try {
    const note = await Note.findById(id);
    if (!note) {
      return res.status(400).json({message: 'No task'});
    }
    req.note = note;
  } catch (err) {
    res.status(500).json({message: err.message});
  }
  next();
};
