const noteSchema = require('../validationSchemes/noteSchema');

module.exports = noteValidationMiddleware = async (req, res, next) => {
  try {
    await noteSchema.validateAsync(req.body);
    next();
  } catch (err) {
    res.status(400).json({message: err});
  }
};
