const {User} = require('../models/userModel');

module.exports = userIsExistsMiddleware = async (username) => {
  const user = await User.findOne({'username': username});
  return user;
};
